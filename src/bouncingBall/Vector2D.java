package bouncingBall;

public class Vector2D {
	public double x = 0, y = 0;
	
	public Vector2D(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	
	public Vector2D scalarMultiple(double scalar)
	{
		return new Vector2D(x*scalar, y*scalar);
	}
	
	public Vector2D sum(Vector2D vec)
	{
		return new Vector2D(x+vec.x, y+vec.y);
	}
	
	public void increment(Vector2D vec)
	{
		this.x += vec.x;
		this.y += vec.y;
	}
	
	public Vector2D normalize()
	{
		return scalarMultiple(getMagnitude());
	}
	
	public double dot(Vector2D vec)
	{
		return vec.x * x + vec.y * y;  
	}

	double getMagnitude()
	{
		return Math.hypot(x, y);
	}
}
