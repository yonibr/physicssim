package bouncingBall;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class BouncingBall extends JPanel {
	public static final int RADIUS = 25;
	private Gravity gravity = new Gravity();
	private Ball ball;

	public BouncingBall(Vector2D startLocation, Vector2D startVelocity) {
		setLayout(null);
		JButton addForceButton = new JButton("Add Force");
		addForceButton.setBounds(10, 600, 300, 40);
		addForceButton.addActionListener(addForceListener);
		add(addForceButton);
		JToggleButton gravityButton = new JToggleButton("Gravity");
		gravityButton.setSelected(false);
		gravityButton.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (((ItemEvent) e).getStateChange() == ItemEvent.SELECTED)
					ball.addForce(gravity);
				else {
					ball.removeForce(gravity);
				}
			}
		});
		gravityButton.setBounds(320, 600, 200, 40);
		add(gravityButton);
		JButton helpButton = new JButton("Help");
		helpButton.setBounds(530, 600, 100, 40);
		helpButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				final String helpMessage = "This is a force simulator. It allows for a particle to be acted on by gravity,\n"
										 + "attractive forces, and repulsive forces. To enable or disable gravity, simply \n"
										 + "press the gravity button. In order to add forces, press the Add Force button. \n"
										 + "This will open up a new window where you can type the pixel location to center\n"
										 + "the force and the strength of the force. The top right corner of the window is\n"
										 + "the point (0,0). Negative strength creates a repulsive force while positive \n"
										 + "strengths create an attractive force."; 
				JOptionPane.showMessageDialog(null, helpMessage, "Help", JOptionPane.INFORMATION_MESSAGE);
				
			}
		});
		add(helpButton);
		ball = new Ball(RADIUS, startLocation, startVelocity);

	}

	public void calculate(float deltaTime) {

		if (ball.getX() < 10) {
			ball.setX(12);
			ball.bounce(new Vector2D(1, 0));
		} else if (ball.getX() > 540) {
			ball.setX(538);
			ball.bounce(new Vector2D(1, 0));
		}
		if (ball.getY() < 11) {
			ball.setY(14);
			ball.bounce(new Vector2D(0, 1));

		} else if (ball.getY() > 540) {
			ball.setY(535);
			ball.bounce(new Vector2D(0, 1));

		}
		ball.calculate(deltaTime);
	}

	public void addForce(Vector2D location, double strength) {
		ball.addForce(new RSquaredField(location, strength));
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D graphics2d = (Graphics2D) g;
		graphics2d.setStroke(new BasicStroke(20));
		graphics2d.setColor(Color.black);
		graphics2d.drawRect(0, 0, 570, 580);
		ball.draw(graphics2d);
	}

	ActionListener addForceListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFrame inputFrame = new JFrame("Add Force");
			inputFrame.setSize(600, 200);
			JPanel inputPanel = new JPanel();
			inputFrame.add(inputPanel);
			JButton okButton = new JButton("Ok");
			JLabel xLabel = new JLabel("x: ");
			JTextField xField = new JTextField();
			xField.setPreferredSize(new Dimension(60, 20));
			JLabel yLabel = new JLabel("y: ");
			JTextField yField = new JTextField();
			yField.setPreferredSize(new Dimension(60, 20));
			JLabel strengthLabel = new JLabel("strength: ");
			JTextField strengthField = new JTextField();
			strengthField.setPreferredSize(new Dimension(60, 20));
			okButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						int x = Integer.parseInt(xField.getText());
						int y = Integer.parseInt(yField.getText());
						double strength = Double.parseDouble(strengthField
								.getText());

						addForce(new Vector2D(x, y), strength);
						inputFrame.dispose();
					} catch (Exception e2) {
					}

				}
			});
			inputPanel.add(xLabel);
			inputPanel.add(xField);
			inputPanel.add(yLabel);
			inputPanel.add(yField);
			inputPanel.add(strengthLabel);
			inputPanel.add(strengthField);
			inputPanel.add(okButton);
			inputFrame.setVisible(true);
		}
	};
}
