package bouncingBall;

import java.awt.Graphics2D;

public abstract class Force {
	Vector2D location;
	double strength;
	
	abstract Vector2D getForceVector(Vector2D callerLocation);
	
	abstract void draw(Graphics2D g2d);
}
