package bouncingBall;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

public class Ball {

	private double radius;
	
	private Vector2D location;
	private Vector2D velocity;
	private ArrayList<Force> forces = new ArrayList<Force>();
	
	public Ball(int radius, Vector2D location, Vector2D velocity)
	{
		this.radius = radius;
		this.location = location;
		this.velocity = velocity;
	}
	

	void calculate(float deltaTime)
	{
		// change velocity
		for (Force force : forces)
		{
			velocity.increment(force.getForceVector(location).scalarMultiple(deltaTime));
		}
		// change location
		location.increment(velocity.scalarMultiple(deltaTime));

	}
	
	double getX()
	{
		return location.x;
	}
	void setX(double x)
	{
		location.x = x;
	}
	
	double getY()
	{
		return location.y;
	}
	
	void setY(double y)
	{
		location.y = y;
	}
	
	public Vector2D getVelocity() {
		return velocity;
	}
	
	void bounce(Vector2D normal)
	{
		double d = normal.dot(velocity) * 2;
		velocity = new Vector2D(velocity.x - d * normal.x, velocity.y - d * normal.y);
	}
	void draw(Graphics2D g2d)
	{
		for (Force force : forces)
		{
			force.draw(g2d);
		}
		
		g2d.setColor(Color.red);
		g2d.fillOval((int) location.x, (int) location.y, (int) radius, (int) radius);
	}
	
	void addForce(Force force)
	{
		forces.add(force);
	}
	
	void removeForce(Force force)
	{
		forces.remove(force);
	}
}
