package bouncingBall;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RadialGradientPaint;

public class RSquaredField extends Force {

	public static final float ALPHA = 0.1f;
	public RSquaredField(Vector2D location, double strength) {
		this.location = location;
		this.strength = strength;
	}

	@Override
	Vector2D getForceVector(Vector2D callerLocation)
	{
		Vector2D direction = location.sum(callerLocation.scalarMultiple(-1));
		double r = direction.getMagnitude();
		direction = direction.normalize();
		return direction.scalarMultiple(strength / r / r);
	}

	@Override
	void draw(Graphics2D g2d) {
		
		int size = (int) Math.abs(strength);
		
		Color color = strength > 0 ? Color.blue : Color.green;
		
		int x = (int) location.x, y = (int) location.y;
		g2d.setPaint(new RadialGradientPaint(new Point(x, y), size, new float []{0, .4f}, new Color[]{color, new Color(0,0,0,0)}));
		g2d.fillOval(x - size / 2, y - size / 2, size, size);
	}

}
