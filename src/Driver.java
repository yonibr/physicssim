import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

import bouncingBall.BouncingBall;
import bouncingBall.Vector2D;

public class Driver extends JFrame {
	BouncingBall bouncingBall = new BouncingBall(new Vector2D(301, 401),
			new Vector2D(0,0));

	public Driver() {
		setVisible(true);
		setBounds(0, 0, 650, 675);
		bouncingBall.setBounds(0, 0, 600, 620);
		add(bouncingBall);

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				bouncingBall.calculate(.017f);
			}
		}, 100, 17);

		while (true) {
			long timeHolder = System.currentTimeMillis();
			repaint();
			if (System.currentTimeMillis() - timeHolder > 1 && System.currentTimeMillis() - timeHolder < 17)
				try {
					Thread.sleep(17 - System.currentTimeMillis() + timeHolder);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		bouncingBall.repaint();
	}

	public static void main(String[] args) {
		new Driver();
	}
}
